" Plugins
" Install vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
" The old masters
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/vim-emoji'
Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-abolish'
" Tweaks
Plug 'reedes/vim-pencil'
Plug 'airblade/vim-gitgutter'
Plug 'dense-analysis/ale'
Plug 'vim-scripts/VimCompletesMe'
Plug 'jremmen/vim-ripgrep'
" Language support
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'rust-lang/rust.vim'
Plug 'tpope/vim-rails'
Plug 'juleswang/css.vim'
Plug 'jparise/vim-graphql'
Plug 'stephpy/vim-yaml'
Plug 'elmcast/elm-vim'
Plug 'tikhomirov/vim-glsl'
Plug 'fgsch/vim-varnish'
Plug 'vmchale/dhall-vim'
Plug 'hashivim/vim-terraform'
Plug 'supercollider/scvim'
Plug 'tidalcycles/vim-tidal'
Plug 'leafgarland/typescript-vim'
Plug 'Quramy/tsuquyomi'
Plug 'jvirtanen/vim-hcl'
" Colors
Plug 'sickill/vim-monokai'
Plug 'sonjapeterson/1989.vim'
Plug 'w0ng/vim-hybrid'
Plug 'junegunn/seoul256.vim'
Plug 'Drogglbecher/vim-moonscape'
Plug 'sjl/badwolf'
Plug 'Lokaltog/vim-distinguished'
Plug 'jnurmine/Zenburn'
Plug 'dracula/vim', { 'as': 'dracula' }
call plug#end()

" General
set nocompatible
set encoding=utf-8
set t_Co=256
set tgc

" Indentations
set backspace=eol,indent,start
filetype plugin indent on
set autoindent
set expandtab
set shiftwidth=2
set softtabstop=2
set tabstop=2
autocmd Filetype vcl setlocal shiftwidth=2
autocmd Filetype elm setlocal shiftwidth=4
autocmd Filetype elm setlocal softtabstop=4
autocmd Filetype elm setlocal tabstop=4

" LEADER
let mapleader=","


" Statusline
set ls=2
set statusline=
set statusline+=‹‹\ (%n)\ %f\ [%{FugitiveHead()}]%=\ %l,%c\ (%L)\ ››


" Filetype detectors
autocmd BufNewFile,BufRead Dockerfile.* set filetype=dockerfile
autocmd BufNewFile,BufRead *.nomad set filetype=hcl

" Tweaks
set scrolloff=1
set relativenumber
set number

syntax on
" Colors
function! RefreshColor()
  colorscheme 1989
  hi Comment cterm=italic
endfunction
call RefreshColor()

" Transparent Background
let t:is_transparent = 0
function! Transparent()
  if t:is_transparent == 0
    hi Normal ctermbg=NONE guibg=NONE
    hi NonText ctermbg=none guibg=NONE
    hi LineNr ctermbg=none guibg=NONE
    let t:is_transparent = 1
  else
    set background=dark
    let t:is_transparent = 0
  endif
endfunction
nnoremap <leader>t :call Transparent()<CR>
call Transparent()

" Relative line numbering
function! RelativeNumbers()
  if &rnu
    set nornu
  else
    set rnu
  endif
endfunction

" Presentation Mode (maybe)
let t:color = 0
function! CycleColor()
  if t:color == 0
    colorscheme moonscape
    let t:color = 1
  elseif t:color == 1
    colorscheme monokai
    let t:color = 2
  elseif t:color == 2
    colorscheme dracula
    let t:color = 3
  elseif t:color == 3
    colorscheme seoul256
    let t:color = 4
  elseif t:color == 4
    colorscheme hybrid
    let t:color = 5
  elseif t:color == 5
    colorscheme seoul256-light
    let t:color = 6
  elseif t:color == 6
    colorscheme zenburn
    let t:color = 7
  elseif t:color == 7
    colorscheme badwolf
    let t:color = 8
  elseif t:color == 8
    colorscheme distinguished
    let t:color = 9
  elseif t:color == 9
    colorscheme 1989
    let t:color = 0
  endif
endfunction

" Mappings
nnoremap <leader>pc :call CycleColor()<CR>
nnoremap <leader>pt :call Transparent()<CR>
nnoremap <leader>pl :call RelativeNumbers()<CR>
nnoremap <leader>ps :syntax sync fromstart<CR>
nnoremap <leader>pz :Goyo<CR>
inoremap jk <Esc>
autocmd FileType typescript nmap <buffer> <leader>h :<C-u>echo tsuquyomi#hint()<CR>
nnoremap <leader>rg :Rg<CR>
" EXPERIMENTAL
nnoremap ; :
vnoremap ; :

" Linting
let g:ale_statusline_format=['💥 %d', '💣 %d', '⬥ ok']
let g:ale_echo_msg_error_str='💥'
let g:ale_echo_msg_warning_str='💣'
let g:ale_echo_msg_format='%severity% %linter%/%code%: %s'

let g:ale_javascript_eslint_suppress_missing_config = 1

nnoremap <leader>ad :ALEDetail<CR>
nnoremap <leader>at :ALEToggle<CR>
nnoremap <leader>ai :ALEInfo<CR>
nnoremap <leader>af :ALEFix<CR>
nnoremap <leader>an :ALENext<CR>
nnoremap <leader>ap :ALEPrevious<CR>

" React
let g:jsx_ext_required = 0

" Rust
augroup rustfmt
  au!
  au BufWritePre *.rs :RustFmt
augroup END

" Emoji
" augroup Emoji
"   au!
"   au InsertLeave *_EDITMSG Emoji
"   au FileType gitcommit set omnifunc=emoji#complete
" augroup END

command! Emoji %s/:\([^:]\+\):/\=emoji#for(submatch(1), submatch(0))/eg

nnoremap <leader>x x~h

" Fugitive leaders
nnoremap <leader>gs :Git<CR>
nnoremap <leader>gc :Git commit<CR>
nnoremap <leader>gb :Git blame<CR>
nnoremap <leader>gd :Git diff<CR>
nnoremap <leader>gl :GBrowse<CR>

" Pencil
let g:pencil#wrapModeDefault = 'soft'
let g:pencil#textwidth = 100 " this doesn't actually work but is 'correct'
augroup pencil
  autocmd!
  autocmd FileType markdown,mkd call pencil#init() | setlocal textwidth=100
  autocmd FileType text         call pencil#init()
augroup END

" Whitespace
function! RemoveTrailingWhitespaceExceptRust()
  if &ft =~ 'rust'
    return
  endif
  let l:winview = winsaveview()
  :%s/\s\+$//e
  call histdel('/', -1)
  call winrestview(l:winview)
endfunction

augroup whitespace
  autocmd!
  autocmd BufWritePre * :call RemoveTrailingWhitespaceExceptRust()
augroup END

command! Curly %s/ -/ \\\r  -/g | %s/;\n/;\r\r/g


function FloatDown()
  norm j
  " until end of file
  while line(".") < line("$")
        \ && (strlen(getline(".")) < col(".")
        \ || getline(".")[col(".") - 1] =~ '\s')
    norm j
  endwhile
endfunction

function FloatUp()
  norm k
  while line(".") > 1
        \ && (strlen(getline(".")) < col(".")
        \ || getline(".")[col(".") - 1] =~ '\s')
    norm k
  endwhile
endfunction

nnoremap <leader>j :call FloatDown()<CR>
nnoremap <leader>k :call FloatUp()<CR>

" Swaps
set backupdir=~/.vim/backup//
set undodir=~/.vim/undo//
set directory=~/.vim/swap//
